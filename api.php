<?php
require_once 'dbConnect.php';
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Headers:Origin, X-Requested-With, Content-type, Accept");

$json = file_get_contents('php://input');
//echo $json;
$res = array();

if ($json != null) {
	$data = json_decode($json);
	if ($data->action === "fetchNotes") {
		$stmt = $con->prepare("SELECT * FROM note");
		$result = $stmt->execute();
		$result = $stmt->get_result();

		/* echo '<br>Result :' . $result . '<br>'; */
		if ($result->num_rows >= 1) {
			$notes = array();
			while ($row = $result->fetch_assoc()) {
				array_push($notes, $row);
			}
			$res['success'] = true;
			$res['data'] = $notes;
		} else if ($result->num_rows == 0) {
			$res['success'] = true;
			$res['message'] = "No Record";
		}
	} else if ($data->action === "addNote") {
		$stmt = $con->prepare("INSERT INTO note (id,title,note) VALUES (?,?,?)");
		$stmt->bind_param('sss', $data->id, $data->title, $data->note);
		$result = $stmt->execute();
		if ($result) {
			$res['success'] = true;
		} else {
			$res['success'] = false;
		}
	} else if ($data->action === "deleteNote") {
		$stmt = $con->prepare("DELETE FROM note where id = ? ");
		$stmt->bind_param('s', $data->id);
		$result = $stmt->execute();
		if ($result) {
			$res['success'] = true;
		} else {
			$res['success'] = false;
		}
	}
	echo json_encode($res);
} else {
	// http_response_code("404");
}
